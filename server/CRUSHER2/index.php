<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width = device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AGGREGATES</title>
    <link rel="stylesheet" href="https://unpkg.com/@ionic/core@latest/css/ionic.bundle.css">
    </link>
    <script src="https://unpkg.com/@ionic/core@latest/dist/ionic.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <!-- <script src="js/jquery-3.4.1.min.js"></script> -->
    <!-- popper всегда сверху -->
    <script src="js/popper.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mdb.min.js"></script>
    <script src="js/stepper.min.js"></script>
    <script type="text/javascript" src="app.js"> </script>

    <script src="node_modules/clipboard/dist/clipboard.min.js"></script>
    <script src="node_modules/html2canvas/dist/html2canvas.min.js"></script>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mdb.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/style/mdb.min.css">
    <link rel="stylesheet" type="text/css" href="/style/main.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body onload="start();">

    <?
        if($_COOKIE['user'] == ''):
        ?>

    <div class="container mt-4" style="padding-top: 50px;" align="center">

        <!-- <div class="row"> -->
        <!-- <div class="col">
                <h1>Форма регистрации</h1>
                <form action="check.php" method="POST">
                    <input type="text" class="form-control" name="login" id="login" placeholder="Введите логин"><br>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Введите имя"><br>
                    <input type="password" class="form-control" name="pass" id="pass" placeholder="Введите пароль"><br>
                    <button class="btn btn-success" type="submit"> Зарегистрировать</button>
                </form>
            </div> -->



        <div class="row">
            <div class="col-4">
            </div>

            <div class="col-4">
                <div class="card card-cascade">
                    <div class="card-body card-body-cascade text-center">

                        <!-- <h1>Форма авторизациии</h1> -->
                        <form action="auth.php" method="POST" style="width: auto; text-align: center!important;">
                            <input type="text" class="form-control" name="login" id="login" placeholder="Введите логин"><br>
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Введите пароль"><br>
                            <button class="btn btn-dark" type="submit" style="width: 100%;"> Войти</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-4">
            </div>
        </div>
    </div>

    <? else:?>

    <script type="text/javascript" src="add_aggregate.js"> </script>
    <script type="text/javascript" src="qrcode.js"></script>





    <!--Navbar -->
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark" style="background-color: rgb(33, 37, 41);">
        <a class="navbar-brand" href="#">AGGREGATES</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item" id="menu_item_0" onclick="click_menu(0)">
                    <a class="nav-link" href="#">Список агрегатов
                        <!-- <span class="sr-only">(current)</span> -->
                    </a>
                </li>
                <li class="nav-item" id="menu_item_1" onclick="click_menu(1)">
                    <a class="nav-link" href="#">Добавить агрегат</a>
                </li>
                <li class="nav-item" id="menu_item_2" onclick="click_menu(2)">
                    <a class="nav-link" href="#">Приложение</a>
                </li>
                <li class="nav-item" id="menu_item_3" onclick="click_menu(3)">
                    <a class="nav-link" href="#">SCADA</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i> <?= $_COOKIE['user'] ?> </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                        <a class="dropdown-item" href="/CRUSHER2/exit.php">Выйти</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--/.Navbar -->


    

    <!-- <a download="" href="system_files/apk/android/steelcom.apk" class="btn btn-primary waves-effect waves-light"> <i class="fas fa-download"></i> Download</a> -->


    <div style="overflow-y: scroll; height: 90%;" id="menu_item_div_0">
        <div style="margin: 25px;">
            <div class="aggregates_items_list">

            </div>
        </div>




        <svg><g id="qrcode" /></svg>
        



    </div>









    <div style="overflow-y: scroll; height: 90%;" id="menu_item_div_1">


        <div class="row" style="margin: 25px;">
            <input type="text" class="form-control" name="unique_name" id="unique_name" placeholder="Введите уникальное имя нового агрегата" style="margin: 15;"><br>
        </div>
        <h5 class="card-title" style="margin: 0px 25px;">Выберите тип агрегата</h5>

        <div class="row" style="margin: 10px;">


            <div class="col-3">
                <div class="card" id="type_aggregate_0" onclick="click_type_aggregate(0)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Стружкодробильный комплекс 1-валковый</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_1" onclick="click_type_aggregate(1)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Стружкодробильный комплекс 2-валковый</p>
                        <!-- <a href="#" class="btn btn-dark" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_2" onclick="click_type_aggregate(2)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 4-валковый</a></h4> -->
                        <p class="card-text">Стружкодробильный комплекс 4-валковый</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_3" onclick="click_type_aggregate(3)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 5-валковый</a></h4> -->
                        <p class="card-text">Стружкодробильный комплекс 5-валковый</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

        </div>


        <div class="row" style="margin: 10px;">


            <div class="col-3">
                <div class="card" id="type_aggregate_4" onclick="click_type_aggregate(4)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Стружкодробильный комплекс 6-валковый</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_5" onclick="click_type_aggregate(5)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Разрывная дробилка 1-валковая</p>
                        <!-- <a href="#" class="btn btn-dark" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_6" onclick="click_type_aggregate(6)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 4-валковый</a></h4> -->
                        <p class="card-text">Разрывная дробилка 2-валковая</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_7" onclick="click_type_aggregate(7)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 5-валковый</a></h4> -->
                        <p class="card-text">Разрывная дробилка 4-валковая</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="margin: 10px;">


            <div class="col-3">
                <div class="card" id="type_aggregate_8" onclick="click_type_aggregate(8)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Разрывная дробилка 5-валковая</p>
                        <!-- <a href="#" class="btn btn-primary" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card" id="type_aggregate_9" onclick="click_type_aggregate(9)">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Card image cap">
                    <div class="card-body">

                        <!-- <h4 class="card-title"><a>Стружкодробильный комплекс 1вал</a></h4> -->
                        <p class="card-text">Разрывная дробилка 6-валковая</p>
                        <!-- <a href="#" class="btn btn-dark" style="width: 100%; margin: 0px;">choose</a> -->
                    </div>
                </div>
            </div>



        </div>


        <div style="margin: 25px;">
            <a href="#" class="btn btn-dark" style="width: 100%;" onclick="add_aggregate_to_db()"> <i class="fas fa-plus-circle"></i> Добавить</a>
        </div>

    </div>



    <? endif;?>


</body>

</html>