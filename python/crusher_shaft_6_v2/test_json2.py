import json
import requests

data = {
        "val_current_relay_1" : 0,
        "val_sensor_speed_1" : 0,
        "overheating_motor_1" : 0,
        "val_clamping_on_1" : 0,
        "val_speed_1" : 0,
        "val_quantity_revers_1" : 0,

        "val_current_relay_2" : 0,
        "val_sensor_speed_2" : 0,
        "overheating_motor_2" : 0,
        "val_clamping_on_2" : 0,
        "val_speed_2" : 0,
        "val_quantity_revers_2" : 0,

        "hammer_on_off" : 0,    
        "hammer_crash_overload" : 0,
        "hammer_speed" : 0,
        "hammer_conveyor_off" : 0,


        # // транспортер выкидной
        "conveyor_on_off_1" : 0,
        "conveyor_current_relay_1" : 0,

        # //  транспортер подающий
        "conveyor_on_off_2" : 0,
        "conveyor_current_relay_2" : 3,
}


out = json.dumps(data)
# print(out)

url = 'http://localhost:8081'


x = requests.post(url, data = data)

print(x.text)