
src = "node_modules/jquery/dist/jquery.min.js"

src = "node_modules/clipboard/dist/clipboard.min.js"

src = "node_modules/html2canvas/dist/html2canvas.min.js"

var unique_name = "";
var type_aggregate = "";
var type_abbreviature = "";


function start() {

    get_aggregates_from_db();

    let col_pages = 4;
    for (let i = 0; i < col_pages; i++) {
        $("#menu_item_" + i).css('background-color', 'rgba(0,0,0,0)');
    }

    $("#menu_item_0").css('background-color', 'rgba(255, 255, 255, 0.3)');

    for (let i = 0; i < col_pages; i++) {
        $("#menu_item_div_" + i).css('display', 'none');
    }

    $("#menu_item_div_0").css('display', 'block');


}

// function click_input(){
//     console.log('_');
//     $("#unique_name").css('border-color', '#dc3545');
//     $("#unique_name").css('box-shadow', ' 0 0 0 0.2rem rgba(220, 53, 69,0.25)');

//     // $("#unique_name").css('border-color', '#ced4da');
//     // $("#unique_name").css('box-shadow', '0 0 0 0.2rem rgba(0,123,255,.25)');
// }


function click_menu(coose_menu_item) {


    let col_pages = 4;
    for (let i = 0; i < col_pages; i++) {
        $("#menu_item_" + i).css('background-color', 'rgba(0,0,0,0)');
    }

    $("#menu_item_" + coose_menu_item).css('background-color', 'rgba(255, 255, 255, 0.3)');
    // console.log("#menu_item_" + coose_menu_item);

    for (let i = 0; i < col_pages; i++) {
        $("#menu_item_div_" + i).css('display', 'none');
    }

    $("#menu_item_div_" + coose_menu_item).css('display', 'block');

    if(coose_menu_item==0){
        get_aggregates_from_db();
    }


}



function click_type_aggregate(coose_aggregate) {
    console.log(coose_aggregate);
    let col_type_aggregate = 10;
    for (let i = 0; i <= col_type_aggregate; i++) {
        $("#type_aggregate_" + i).css('opacity', '1');
    }

    switch (coose_aggregate) {
        case 0:
            type_aggregate = "Стружкодробильный комплекс 1-валковый";
            type_abbreviature  = "complex_1";
            break;
        case 1:
            type_aggregate = "Стружкодробильный комплекс 2-валковый";
            type_abbreviature  = "complex_2";
            break;
        case 2:
            type_aggregate = "Стружкодробильный комплекс 4-валковый";
            type_abbreviature  = "complex_4";
            break;
        case 3:
            type_aggregate = "Стружкодробильный комплекс 5-валковый";
            type_abbreviature  = "complex_5";
            break;
        case 4:
            type_aggregate = "Стружкодробильный комплекс 6-валковый";
            type_abbreviature  = "complex_6";
            break;

        case 5:
            type_aggregate = "Разрывная дробилка 1-валковая";
            type_abbreviature  = "bursting_1";
            break;
        case 6:
            type_aggregate = "Разрывная дробилка 2-валковая";
            type_abbreviature  = "bursting_2";
            break;
        case 7:
            type_aggregate = "Разрывная дробилка 4-валковая";
            type_abbreviature  = "bursting_4";
            break;
        case 8:
            type_aggregate = "Разрывная дробилка 5-валковая";
            type_abbreviature  = "bursting_5";
            break;
        case 9:
            type_aggregate = "Разрывная дробилка 6-валковая";
            type_abbreviature  = "bursting_6";
            break;


    }
    console.log(type_aggregate)

    $("#type_aggregate_" + coose_aggregate).css('opacity', '0.5');
}


function add_aggregate_to_db() {
    unique_name = document.getElementById("unique_name").value;

    if (unique_name == "" && type_aggregate == "") {
        alert("Введите уникальное имя нового агрегата и выберите тип агрегата")
    }
    else if (unique_name == "") {
        alert("Введите уникальное имя нового агрегата")
    }
    else if (type_aggregate == "") {
        alert("выберите тип агрегата")
    }

    let time_seconds = new Date().getTime() / 1000;
    console.log(time_seconds);

    $.ajax({
        type: 'POST',
        url: "http://188.225.39.107/CRUSHER2/aggregate.php",
        dataType: 'text',
        data: {
            'action': "add_aggregate",
            'unique_name': unique_name,
            'type_aggregate': type_aggregate,
            'type_abbreviature' : type_abbreviature,
            'time': time_seconds
        },
        async: false,
        success: function (data) {
            console.log(data);
            alert(data)
        },
        error: function () {
            alert('Error occured');
        }
    });

}




function get_aggregates_from_db() {


    let time_seconds = new Date().getTime() / 1000;
    console.log(time_seconds);

    aggregates = get_data();
    aggregates = aggregates['aggregates']
    console.log(aggregates)

    // $('.aggregates_items_list').detach();
    $('.aggregates_items_list').text('');

    for (let i = 0; i < aggregates.length; i++) {
        unique_name = aggregates[i][0];
        type_aggregate = aggregates[i][1];
        qr_code = aggregates[i][2];
        time = aggregates[i][3];

        date = new Date(null);
        date.setSeconds(time);
        day = date.toISOString().substr(0, 10);
        time = date.toISOString().substr(11, 8);
        time = day + " " + time;


        console.log(i);
        $('.aggregates_items_list').append('<div class="row" >' +
            '<p style="text-align: center;" ><img  src="https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"> ' +
            '</p> <ion-label class="sc-ion-label-ios-h sc-ion-label-ios-s ios hydrated" style="margin-left: 20px;"">  ' +
            '<h2>' + unique_name + '</h2>  <h4>' + type_aggregate + '</h4>   <p>' + time + '</p>  </ion-label> ' +

            '<ul class="navbar-nav ml-auto">' +

            '<li class="nav-item dropdown">' +
            ' <a class="nav-link dropdown-toggle"  id="qrcodeLink' + i + '" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"> ' +
            ' <i  class="fas fa-qrcode fa-3x"></i> </a>' +

            ' <div class="dropdown-menu"   style="text-align: center;" aria-labelledby="qrcodeLink' + i + '">' +
            '<img  src="http://qrcoder.ru/code/?'+ qr_code +'&10&0" >  ' +
            // ' <button class="btn btn-dark" style="width: 100%; margin: 0px; margin-top: 5px;" id="btn_copyqrcode' + i + '" alt="Copy to clipboard"> <i class="far fa-copy"></i> Copy</button>' +
            ' </div> ' +
            '</li>' +
            '</ul>' +

            '</div>  ')






    }

}




function get_data() {
    $.ajax({
        type: 'POST',
        url: "http://188.225.39.107/CRUSHER2/aggregate.php",
        dataType: 'text',
        data: {
            'action': "get_aggregates",
        },
        async: false,
        success: function (data) {
            // console.log(data);
            data = JSON.parse(data);
            result = data;
            // console.log(data["aggregates"]);
        },
        error: function () {
            console.log('Error occured');
        }
    });
    return result;
}



// var qrcode = new QRCode(document.getElementById("qrcode"), {
//     width: 100,
//     height: 100,
//     useSVG: true
// });

// qrcode.makeCode("3");

// function makeCode() {

//     qrcode.makeCode("3");
// }

// makeCode();


