import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Complex1Page } from './complex1.page';

const routes: Routes = [
  {
    path: '',
    component: Complex1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Complex1PageRoutingModule {}
