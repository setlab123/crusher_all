import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-complex1',
  templateUrl: './complex1.page.html',
  styleUrls: ['./complex1.page.scss'],
})
export class Complex1Page implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient) { }

  unique_name = ""
  aggregates_info = []

  // gap_array = [];
  // hamer_array = [];
  // conveyor1_array = [];
  // conveyor2_array = [];

  gap_array: Array<any> = [];
  hamer_array: Array<any> = [];
  conveyor1_array: Array<any> = [];
  conveyor2_array: Array<any> = [];




  async ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log('params ', params);
      this.unique_name = params["unique_name"]
      console.log(this.unique_name)
    });

    await this.get_aggregate_data()

  }

  async get_aggregate_data() {

    let postData = new FormData();
    postData.append('action', 'get_aggregate_data');
    // this.unique_name = 'test1';
    postData.append('table', this.unique_name);

    this.http.post('http://188.225.39.107/CRUSHER2/aggregate.php', postData).subscribe(data => {
      // console.log(data);
      let aggregate = data["data"]
      console.log(aggregate);
      this.aggregates_info = aggregate

      this.gap_array = [];
      this.hamer_array = [];
      this.conveyor1_array = [];
      this.conveyor2_array = [];

      let gap = this.aggregates_info[0]['gap'];

      let count_gap = Object.keys(gap);
      for (let i = 0; i < count_gap.length; i++) {
        this.gap_array.push(this.aggregates_info[0]['gap'][count_gap[i]]);
      }

      // this.gap_array.push(this.aggregates_info[0]['gap'])

      this.hamer_array.push(this.aggregates_info[0]['hamer'])
      this.conveyor1_array.push(this.aggregates_info[0]['conveyor1'])
      this.conveyor2_array.push(this.aggregates_info[0]['conveyor2'])

      console.log(this.hamer_array[0].hammer_speed)

    

    }, err => {
      console.log(err)
    });


  }



}
