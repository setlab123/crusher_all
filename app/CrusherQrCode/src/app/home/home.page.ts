import { Component } from '@angular/core';
import { BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  scannedData: {};
  barcode: string;
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(private barcodeScanner: BarcodeScanner,
    private router: Router,
    private storage: Storage,
    private http: HttpClient) {

    this.barcode = "";
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }

  qrcodes_array = []

  aggregates_info = []



  async ngOnInit() {
    await this.storage.get('aggregates').then((val) => {
      if (val == undefined) {
        console.log("null :", val);
      } else {
        console.log("aggregates :", val);
        this.qrcodes_array = JSON.parse(val)['aggregates']
        console.log(this.qrcodes_array);
      }

      // this.storage.set('path', JSON.stringify(json_path_data))
    })

    await this.get_aggregates();

  }


  async scanBarcode() {
    this.barcodeScanner.scan().then(async barcodeData => {
      this.scannedData = barcodeData;
      this.barcode = barcodeData.text;

      this.qrcodes_array.push(this.barcode);
      this.qrcodes_array = this.unique(this.qrcodes_array);
      console.log(this.qrcodes_array)

      let json_qrcodes_array = {
        aggregates: this.qrcodes_array
      }
      this.storage.set('aggregates', JSON.stringify(json_qrcodes_array))

      await this.get_aggregates();

    }).catch(err => {
      alert(err);
    });



  }


  showDetail(aggregate_info) {
    console.log("showDetail");
    let type_abbreviature = aggregate_info[0][4]
    let unique_name = aggregate_info[0][0]
    console.log(type_abbreviature);

    let navigationExtras = {
      queryParams: {
        unique_name : unique_name,

      }
    }
    // let type_abbreviature = ""
    let navigate_page = ""
    switch (type_abbreviature) {
      case "complex_1":
        navigate_page = "complex1"
        break;
      case "complex_2":
        navigate_page = "complex2"
        break;
      // case "complex_3":
      //   navigate_page = "complex3"
      //   break;
      case "complex_4":
        navigate_page = "complex4"
        break;
      case "complex_5":
        navigate_page = "complex5"
        break;
      case "complex_6":
        navigate_page = "complex6"
        break;
    }

    this.router.navigate([navigate_page], navigationExtras);
  }



  async get_aggregates() {
    this.aggregates_info = []
    for (let i = 0; i < this.qrcodes_array.length; i++) {
      console.log(this.qrcodes_array[i])

      let postData = new FormData();
      postData.append('action', 'get_aggergates_by_qr_code');
      postData.append('qr_code', this.qrcodes_array[i]);

      this.http.post('http://188.225.39.107/CRUSHER2/aggregate.php', postData).subscribe(data => {
        console.log(data);
        let aggregate = data["aggregate"]
        console.log(aggregate);
        this.aggregates_info.push(aggregate)

      }, err => {
        console.log(err)
      });

    }
    console.log(this.aggregates_info)
  }


  unique(arr) {
    let result = [];
    for (let str of arr) {
      if (!result.includes(str)) {
        result.push(str);
      }
    }
    return result;
  }



}
