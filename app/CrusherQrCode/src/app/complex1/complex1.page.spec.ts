import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Complex1Page } from './complex1.page';

describe('Complex1Page', () => {
  let component: Complex1Page;
  let fixture: ComponentFixture<Complex1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Complex1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Complex1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
