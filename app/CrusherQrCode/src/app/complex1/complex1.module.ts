import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Complex1PageRoutingModule } from './complex1-routing.module';

import { Complex1Page } from './complex1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Complex1PageRoutingModule
  ],
  declarations: [Complex1Page]
})
export class Complex1PageModule {}
